// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.List;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrapezoidProfile.Constraints;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import frc.robot.subsystems.Drivetrain;

public class AlignToTargetButBetter extends CommandBase {

  private SwerveControllerCommand swerveControllerCommand;

  private Drivetrain drivetrain;

  /** Creates a new AlignToTargetButBetter. */
  public AlignToTargetButBetter(Drivetrain drivetrain) {
    this.drivetrain = drivetrain;
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    ProfiledPIDController controller = new ProfiledPIDController(0.2, 0, 0, new Constraints(0.5, 4));
    controller.enableContinuousInput(-Math.PI, Math.PI);

    swerveControllerCommand = new SwerveControllerCommand(
        TrajectoryGenerator.generateTrajectory(
          // Starting location
          drivetrain.getPoseEstimator().getEstimatedPosition(),
          // no intermediate waypoints
          List.of(),
          // End 1 meter in front of the target
          new Pose2d(1,0,Rotation2d.fromDegrees(0)),
          // drivetrain.getPoseEstimator().getEstimatedPosition().transformBy(new Transform2d(new Translation2d(-1, 0), new Rotation2d()).times(-1)),
          new TrajectoryConfig(
                      Drivetrain.MAX_VELOCITY_METERS_PER_SECOND,
                      4)
                  .setKinematics(drivetrain.getKinematics())
          ),
          () -> drivetrain.getPoseEstimator().getEstimatedPosition(),
          drivetrain.getKinematics(),
          new PIDController(0.3, 0, 0.1), // 0.2 0.0 0.0
          new PIDController(0.3, 0, 0.1), // 0.2 0.0 0.0
          controller,
          (states) -> drivetrain.setModuleStates(states));
    swerveControllerCommand.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    swerveControllerCommand.execute();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    swerveControllerCommand.end(interrupted);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return swerveControllerCommand.isFinished();
  }
}
