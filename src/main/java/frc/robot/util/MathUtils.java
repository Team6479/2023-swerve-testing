package frc.robot.util;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;

public class MathUtils {
    public static Pose2d pose3dTo2d(Pose3d pose3d) {
        return new Pose2d(pose3d.getX(), pose3d.getY(), Rotation2d.fromRadians(pose3d.getRotation().getZ()));
    }

    public static Pose3d pose2dTo3d(Pose2d pose2d) {
        return new Pose3d(pose2d.getX(), pose2d.getY(), 0, new Rotation3d(0, 0, pose2d.getRotation().getRadians()));
    }
}
